# aws (DEPRECATED)

This repo is deprecated.

The code has been moved to these repos:

- https://gitlab.com/devopscoop/scripts
- https://codeberg.org/devopscoop/scripts
- https://github.com/devopscoop/scripts
